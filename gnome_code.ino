const double MAX_ANALOG_VALUE = 1023;
int potentiometerPin = 26;
int sensorPin = 28;
int pumpPin = 2;

void setup() 
{
  Serial.begin(9600);
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(pumpPin, OUTPUT);
}

void loop() 
{
  double potentiometerValue = analogRead(potentiometerPin) / MAX_ANALOG_VALUE;
  double sensorValue = analogRead(sensorPin) / MAX_ANALOG_VALUE;

  Serial.print("Sensor value: ");
  Serial.print(sensorValue);
  Serial.print("     Pot value: ");
  Serial.println(potentiometerValue);

  if (sensorValue < potentiometerValue)
  {
    digitalWrite(pumpPin, HIGH);
  }
  else
  {
    digitalWrite(pumpPin, LOW);
  }

  digitalWrite(LED_BUILTIN, HIGH);
  delay(100);
  digitalWrite(LED_BUILTIN, LOW);
  delay(100);
}
